/*
var http = require('http');

http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('Hello, world! [helloworld sample; iisnode version is ' + process.env.IISNODE_VERSION + ', node version is ' + process.version + ']');
}).listen(process.env.PORT);  
*/
var $tw = {};

require("./node_modules/tiddlywiki/boot/bootprefix.js").bootprefix($tw)

// Dummy command line arguments telling TW5 not to load a wiki from the filesystem
$tw.boot = $tw.boot || {};
$tw.boot.argv = ["\\Test Sites\\Front Ends\\AllStarMisc\\KnuckleheadCafe\\", "--server", process.env.PORT||8888, "HomePageTemplatedHTML", "text/plain", "text/html", "", "", "127.0.0.1", "/AllStarMisc/KnuckleheadCafe"];

//--server <port> <roottiddler> <rendertype> <servetype> <username> <password> <host> <pathprefix>

 
 
require("./node_modules/tiddlywiki/boot/boot.js").TiddlyWiki($tw);

// Boot TiddlyWiki
$tw.boot.boot();

// Add some tiddlers
//$tw.wiki.addTiddler({title: "TiddlerOne", text: "Text of tiddler one, incorporating the {{TiddlerTwo}}", tags: ["alpha", "beta"]});
//$tw.wiki.addTiddler({title: "TiddlerTwo", text: "Text of tiddler two"});

// Render a tiddler as HTML
//var html = $tw.wiki.renderTiddler("text/html","TiddlerOne");
//console.log(html);  